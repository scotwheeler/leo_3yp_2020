The data contained in OxEMF_3YP is example energy system data.

### solar02Jan.csv
Example winter day solar data obtained from [www.renewables.ninja](www.renewables.ninja).

Half hourly data, values are in kWh.

### wnt_wd.csv
Example weekday winter demand profile for the UK, obtained from [UKERC Energy Data Centre](http://ukerc.rl.ac.uk/cgi-bin/era001.pl?GoButton=EResult&STerm=elexon&SScope=&GoAct=&AFull=5&EWCompID=42&AllFilters=&RandKey=&TotHead=5%20results%20for%20%E2%80%9Celexon%E2%80%9D%20**)

Half hourly data, values are in kW.

### mip.csv
Example half hourly energy imbalance price from [Elexon](www.elexonportal.co.uk)

col 3: HH settlement period

col 4: System buy/sell price £/MWh

col 5: Net imbalance volume MWh
