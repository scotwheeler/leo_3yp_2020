# -*- coding: utf-8 -*-

"""
3YP main setup file.
Adapted from UoO EPG's energy management framework.
Authors: Avinash Vijay, Scot Wheeler
"""

__version__ = '0.3'

# import modules

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib.dates as mdates
import datetime

import Assets as AS
import EnergySystem as ES
import Market as MK


#######################################
### STEP 1: setup parameters
#######################################

dt = 30/60  # 30 minute time intervals
T = int((24 * 365) / dt)  # Number of intervals

#######################################
### STEP 2: setup the assets
#######################################

dispatchable = []
non_dispatchable = []
all_assets = []

# PV source
pv_capacity = 30  # kW
pv_site1 = AS.pvAsset(pv_capacity)
non_dispatchable.append(pv_site1)

# Load
nHouses = 10
load_site1 = AS.loadAsset(nHouses)
non_dispatchable.append(load_site1)

# Battery
battery_capacity = 100  # kWh
battery_power = 25  # kW
battery_eff = 0.8
#battery_site1 = AS.IdealBatteryAsset(battery_capacity, battery_power, dt, T)
battery_site1 = AS.PracticalBatteryAsset(battery_capacity, battery_power,
                                         battery_eff, dt, T)
dispatchable.append(battery_site1)



#######################################
#STEP 4: setup and run the energy system
#######################################

all_assets = non_dispatchable + dispatchable

# setup
energy_system = ES.EnergySystem(non_dispatchable, dispatchable, dt, T)
# run
net_load = energy_system.basic_energy_balance()

#######################################
### STEP 6: setup and run the market
#######################################

# setup
market1 = MK.marketObject(energy_system, datetime.datetime(2017, 1, 1),
                          datetime.datetime(2017, 12, 31, 23, 59, 59))
# run
opCost = market1.getTotalCost()
grid_cost = market1.getGridCost().sum()
print('Operating cost: £ %3.3f' % (grid_cost / 100))

purchased, sold = market1.gridBreakdown()

# =============================================================================
# Daily
# =============================================================================
pv_daily = ES.E_to_dailyE(pv_site1.output, dt)
load_daily = ES.E_to_dailyE(load_site1.output, dt)
purchased_daily = ES.E_to_dailyE(purchased, dt)
sold_daily = ES.E_to_dailyE(sold, dt)

# =============================================================================
# grid search
# =============================================================================
#pv_cap = range(1, 200, 10)
#system_cost = []
#
#for pv_capacity in pv_cap:
#    pv_site1 = AS.pvAsset(pv_capacity)
#    non_dispatchable[0] = pv_site1
#    all_assets = non_dispatchable + dispatchable
#    energy_system = ES.EnergySystem(non_dispatchable, dispatchable, dt, T)
#    net_load = energy_system.basic_energy_balance()
#    market1 = MK.marketObject(datetime.datetime(2017,1,1),
#                              datetime.datetime(2017,12,31,23,59,59),
#                              FiT=0)
#    opCost = market1.getTotalCost(net_load, all_assets)
#    system_cost.append(opCost)

# #######################################
# ### STEP 7: plot results
# #######################################

#ax = plt.subplot(1,1,1)
#p1 = plt.plot(pv_cap, system_cost)
#plt.xlabel("PV size (kWp)")
#plt.ylabel("Annual system cost (£)")
#plt.show()

ax = plt.subplot(1,1,1)
p1 = plt.bar(range(365), purchased_daily)
p2 = plt.bar(range(365), sold_daily)
#plt.xlabel("PV size (kWp)")
#plt.ylabel("Annual system cost (£)")
plt.show()

#plt.stackplot(np.array(range(48)), pv_site1.output, hydro_site1.output)
#x_axis = pd.date_range(datetime.datetime(2017,1,1), datetime.datetime(2017, 12, 31, 23, 59, 59), freq='0.5H')
#labels = ['PV Output', 'Hydro Operation']
#ax = plt.subplot(1,1,1)
#p1 = plt.stackplot(x_axis, pv_site1.output.T, battery_site1.output.T, labels=labels)
#p2 = plt.plot(x_axis, net_load, '-k', label='Load')
##plt.xticks(range(365*48, (365*48)/12))
##ax.set_xticklabels(['00:00','06:00','12:00','18:00','00:00'])
#plt.ylabel('(kWh)', color='k')
#plt.xlabel('Time', color='k')
#ax.legend()
#ax.format_xdata = mdates.DateFormatter('%m')
##ax.autofmt_xdate()
##plt.legend()
##plt.plot(battery_site1.soc/battery_site1.capacity)
#plt.show()
#plt.pause(0.01)